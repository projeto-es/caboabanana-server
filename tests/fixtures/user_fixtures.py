from caboabanana.user.models import User
import pytest


@pytest.fixture(scope='function')
def user(database):
    user = User("123", "foo", "foo@foo.com", "boo")
    database.session.add(user)
    database.session.commit()
    return user
