import pytest
from caboabanana.list_items.item.models import Item


@pytest.fixture(scope='function')
def list_items(database, group_user):
    result = group_user.group.list_items
    user = group_user.user
    result.items.append(
        Item(False, 'banana', 1, '', 10, user, result))
    database.session.commit()
    return result
