from caboabanana.group_user.models import GroupUser
import pytest


@pytest.fixture(scope='function')
def group_user(database, user, group):
    association = GroupUser(user, group)
    group.members.append(association)
    database.session.commit()
    return association
