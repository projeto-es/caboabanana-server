from caboabanana.group.models import Group
from caboabanana.list_items.models import ListItems
import pytest


@pytest.fixture(scope='function')
def group(database, user):
    group = Group(None, "family", "blue", user)
    database.session.add(group)
    database.session.commit()
    group.list_items = ListItems(group)
    database.session.commit()
    return group
