from caboabanana.invitation.models import Invitation
from caboabanana.user.models import User
import pytest


@pytest.fixture(scope='function')
def invitation(database, user, group):
    session = database.session
    remettee = User("1234", "foo", "foo@foo.com", "boo")
    session.add(user)
    invite = Invitation(user, remettee, group)
    group.invitations.append(invite)
    session.add(group)
    session.commit()
    return invite
