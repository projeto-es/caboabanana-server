def test_login_required(user, client):
    rv = client.post('/secure', headers={'Authorization': user.uid})
    assert rv.get_json() == {'username': user.name}


def test_login_unathorized(client):
    rv = client.post('/secure', headers={'Authorization': ''})
    assert rv.get_json() == {
        'type': 'UnauthorizedRequest',
        'message': 'Invalid token'
    }
