def test_insert_user(client):
    rv = client.post(
        '/users/',
        json={
            'name': 'ola',
            'email': 'ola@ola.com',
            'password': 'olaolaola'
        })
    assert rv.get_json() == {
        'uid': '1',
        'name': 'ola',
        'email': 'ola@ola.com',
        'access_adress': ''
    }


def test_insert_little_password(client):
    rv = client.post(
        '/users/',
        json={
            'name': 'ola',
            'email': 'ola@ola.com',
            'password': 'ol'
        })
    assert rv.get_json() == {
        "message": "Password may not have less than 6 characteres.",
        "type": "ValidationException"
    }


def test_insert_invalid_email(client):
    rv = client.post(
        '/users/', json={
            'name': 'ola',
            'email': 'ola',
            'password': 'password'
        })
    assert rv.get_json() == {
        "message": "{'email': ['Not a valid email address.']}",
        "type": "ValidationException"
    }


def test_insert_little_name(client):
    rv = client.post(
        '/users/',
        json={
            'name': 'o',
            'email': 'ola@ola.com',
            'password': 'password'
        })
    assert rv.get_json() == {
        "message": "Name may not have less than 3 characteres.",
        "type": "ValidationException"
    }


def test_update_user_access_adress(client, user):
    """
    GIVEN a PUT request
    WHEN sent the request with especif JSON
    THEN check if the access adress of the request return matchs
    the sent access adress
    """
    access_adress = "access_adress"
    rv = client.put(
        '/users/',
        headers={'Authorization': user.uid},
        json={
            'access_adress': access_adress
        })
    assert rv.get_json()[access_adress] == access_adress
