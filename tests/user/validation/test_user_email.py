import pytest
from caboabanana.user.models import user_schema
from caboabanana.errors.exceptions import ValidationException


@pytest.mark.parametrize("email, message", [
    (None, "Field may not be null."),
    ("   ", "Email may not be empty."),
    ("a"*150+"@a.com", "Email may not have more than 150 characteres.")
])
def test_name(email, message):
    """
    GIVEN a User with an invalid email
    WHEN the User is loaded
    THEN check if Exception matchs
    """
    with pytest.raises(ValidationException, match=message):
        user_schema.load({
            "name": "banana",
            "email": email,
            "password": "banana",
        })


@pytest.mark.parametrize("email", [
    ("banana"),
    ("banana@"),
    ("banana.com"),
    ("@banana.com"),
    ("banana@banana@banana.com")
])
def test_email_format_validation(email):
    """
    GIVEN a User with invalid email
    WHEN the User is loaded
    THEN check if Exception matchs
    """
    with pytest.raises(ValidationException,
                       match="Not a valid email address."):
        user_schema.load({
            "name": "banana",
            "email": email,
            "password": "banana",
        })
