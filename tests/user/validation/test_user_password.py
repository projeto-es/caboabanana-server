import pytest
from caboabanana.user.models import user_schema
from caboabanana.errors.exceptions import ValidationException


@pytest.mark.parametrize("password, message", [
    (None, "Field may not be null."),
    ("   ", "Password may not be empty."),
    ("a"*5, "Password may not have less than 6 characteres."),
    ("a"*41, "Password may not have more than 40 characteres.")
])
def test_name(password, message):
    """
    GIVEN a User with an invalid password
    WHEN the User is loaded
    THEN check if Exception matchs
    """
    with pytest.raises(ValidationException, match=message):
        user_schema.load({
            "name": "banana",
            "email": "banana@banana.com",
            "password": password,
        })
