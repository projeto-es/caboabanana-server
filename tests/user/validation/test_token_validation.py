import pytest
from caboabanana.user.validation_token import validate_token
from caboabanana.errors.exceptions import UnauthorizedRequest


def test_invalid_token(database):
    with pytest.raises(UnauthorizedRequest, match="Invalid token"):
        validate_token("2")


def test_valid_token(database, user):
    validated_user = validate_token(user.uid)

    assert validated_user.uid == user.uid
    assert validated_user.name == user.name
    assert validated_user.email == user.email
