from caboabanana.user.models import user_schema
from caboabanana.user.models import User


def test_load_user():
    """
    GIVEN a JSON of a User and the UserSchema
    WHEN the User is loaded
    THEN check if no Exception is thrown
    """
    user_schema.load({
        "name": "banana",
        "email": "banana@banana.com",
        "password": "banana",
    })


def test_dump_user():
    """
    GIVEN a User
    WHEN the User is dumped
    THEN check if there are no erros and data matchs
    """
    user = User("1234567910", "banana", "banana@banana.com", "banana")
    data, errors = user_schema.dump(user)
    assert data == {"email": "banana@banana.com",
                    "name": "banana",
                    "uid": "1234567910",
                    "access_adress": ""}
    assert errors == {}
