from sqlalchemy.orm.exc import FlushError
from caboabanana.user.models import User
import pytest


def test_insert_user(database):
    """
    GIVEN a User model
    WHEN a new User is created
    THEN ensures that this user is persisted in database.
    """
    user = User("fooId", "foo", "foo@foo.com", "boo")
    database.session.add(user)
    database.session.commit()
    inserted_user = User.query.get("fooId")

    assert inserted_user.uid == user.uid
    assert inserted_user.name == user.name
    assert inserted_user.email == user.email

    other_user = User("fooId1", "foo", "foo@foo.com", "boo")
    assert other_user != inserted_user


def test_not_insert_repeted_user(database):
    """
    GIVEN a User model
    WHEN a new User with repeated uid is created
    THEN ensures that this user isn't persisted in database.
    """
    user = User("1", "foo", "foo@foo.com", "boo")
    database.session.add(user)
    database.session.commit()
    database.session.add(User("1", "f", "f", "b"))
    with pytest.raises(FlushError):
        database.session.commit()
