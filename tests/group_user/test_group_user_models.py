from caboabanana.group_user.models import GroupUser
from caboabanana.group.models import Group


def test_insert_group_user(database, user, group):
    """
    GIVEN a GroupUser model
    WHEN a new GroupUser is created
    THEN ensures that this groupUser is persisted in database.
    """
    session = database.session
    assert len(Group.query.get(group.uid).members) == 0
    a = GroupUser(user, group)
    user.groups.append(a)
    session.commit()
    assert len(Group.query.get(group.uid).members) == 1


def test_not_insert_repeted_user(database, user, group):
    """
    GIVEN a GroupUser model
    WHEN a new Group with user is created
    THEN ensures that this group isn't persisted with the same user.
    """
    session = database.session
    a = GroupUser(user, group)
    user.groups.append(a)
    session.add(user)
    session.commit()

    user.groups.append(a)
    session.add(user)
    session.commit()
    assert len(Group.query.get(group.uid).members) == 1
