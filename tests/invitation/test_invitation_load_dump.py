from caboabanana.invitation.models import invitation_schema, Invitation


def test_load_inivitation():
    """
    GIVEN a JSON of an Invitation and the InvitationSchema
    WHEN the Invitation is loaded
    THEN check if no Exception is thrown
    """
    invitation_schema.load({"uid_sender": "fooId", "uid_group": "fooId"})


def test_dump_invitation(user, group):
    """
    GIVEN an Invitation
    WHEN the Invitation is dumped
    THEN check if there are no erros and data matchs
    """
    invitation = Invitation(user, user, group)
    data, errors = invitation_schema.dump(invitation)
    assert data == {
        "group": {
            "author": {
                "email": "foo@foo.com",
                "name": "foo",
                "uid": "123",
                "access_adress": ""
            },
            "color": "blue",
            "name": "family",
            "uid": 1},
        "remittee": {
            "email": "foo@foo.com",
            "name": "foo",
            "uid": "123",
            "access_adress": ""},
        "sender": {
            "email": "foo@foo.com",
            "name": "foo",
            "uid": "123",
            "access_adress": ""}}
    assert errors == {}
