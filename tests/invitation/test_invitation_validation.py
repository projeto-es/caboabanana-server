import pytest
from caboabanana.invitation.models import invitation_schema
from caboabanana.errors.exceptions import ValidationException


@pytest.mark.parametrize(
    "sender, group",
    [(None, "fooId"),
     ("fooId", None)]
)
def test_invitation_null_uid_sender_group(sender, group):
    """
    GIVEN an Invitation with a Null sender uid
    WHEN the Invitation is loaded
    THEN check if Exception matchs
    """
    with pytest.raises(ValidationException, match="Field may not be null"):
        invitation_schema.load({
            "uid_sender": sender,
            "uid_group": group,
        })
