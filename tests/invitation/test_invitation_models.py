from caboabanana.invitation.models import Invitation
from caboabanana.group.models import Group
from caboabanana.user.models import User


def test_insert_invitation(database, user, group):
    """
    GIVEN a Invitation model
    WHEN a new Invitation is created
    THEN ensures that this invitation is persisted in database.
    """
    session = database.session
    assert len(Group.query.get(group.uid).invitations) == 0
    remettee = User("1234", "foo", "foo@foo.com", "boo")
    database.session.add(user)
    database.session.commit()

    invite = Invitation(user, remettee, group)
    group.invitations.append(invite)
    session.commit()
    assert len(Group.query.get(group.uid).invitations) == 1


def test_change_status_invitation(database, invitation, group):
    """
    GIVEN a Invitation created
    WHEN a User accepted that Invitation
    THEN ensures that this invitation is accepted in database.
    """
    assert len(group.invitations) == 1
    assert not group.invitations[0].accepted
    session = database.session
    invitation.accepted = True
    session.add(invitation)
    session.commit()
    group = Group.query.get(group.uid)
    assert len(group.invitations) == 1
    assert group.invitations[0].accepted
