import pytest
from caboabanana.list_items.item.models import item_schema
from caboabanana.errors.exceptions import ValidationException


@pytest.mark.parametrize("quantity, message", [
    (None, "Field may not be null."),
    (0, "Quantity may not be zero."),
    (-1, "Quantity may not be negative.")
])
def test_quantity(quantity, message):
    """
    GIVEN a Item with an invalid quantity
    WHEN the Item is loaded
    THEN check if Exception matchs
    """
    with pytest.raises(ValidationException, match=message):
        item_schema.load({
            "name": "banana",
            "quantity": quantity,
            "isIndividual": True,
        })
