import pytest
from caboabanana.list_items.item.models import item_schema
from caboabanana.errors.exceptions import ValidationException


def test_note():
    """
    GIVEN a Item with an invalid individual attribute
    WHEN the Item is loaded
    THEN check if Exception matchs
    """
    with pytest.raises(ValidationException, match="Field may not be null."):
        item_schema.load({
            "name": "banana",
            "quantity": 1,
            "isIndividual": None,
        })
