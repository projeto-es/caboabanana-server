import pytest
from caboabanana.list_items.item.models import item_schema
from caboabanana.errors.exceptions import ValidationException


@pytest.mark.parametrize("name, message", [
    (None, "Field may not be null."),
    ("   ", "Name may not be empty."),
    ("a"*41, "Name may not have more than 40 characteres."),
    ("a"*2, "Name may not have less than 3 characteres.")
])
def test_name(name, message):
    """
    GIVEN a Item with an invalid name
    WHEN the Item is loaded
    THEN check if Exception matchs
    """
    with pytest.raises(ValidationException, match=message):
        item_schema.load({
            "name": name,
            "quantity": 1,
            "isIndividual": True,
        })
