from caboabanana.list_items.item.models import item_schema, Item
from caboabanana.user.models import User
from caboabanana.group.models import Group
from caboabanana.list_items.models import ListItems


def test_load_item():
    """
    GIVEN a JSON of an Item and the ItemSchema
    WHEN the Item is loaded
    THEN check if no Exception is thrown
    """
    item_schema.load({
        "name": "banana",
        "quantity": 1,
        "isIndividual": False,
    })


def test_dump_item():
    """
    GIVEN an Item
    WHEN the Item is dumped
    THEN check if there are no erros and data matchs
    """
    author = User("123", "foo", "foo@foo.com", "boo")
    group = Group(1234567, "banana", "#000000", author)
    group.list_items = ListItems(group)
    group.list_items.items.append(
        Item(False, 'banana', 1, 'note', 0, group.author, group.list_items, 1))
    item = group.list_items.items[0]
    data, errors = item_schema.dump(item)
    assert data == {
        "uid": 1,
        "name": "banana",
        "quantity": 1,
        "note": "note",
        "maxPrice": 0,
        'user': {
            'access_adress': '',
            'email': 'foo@foo.com',
            'name': 'foo',
            'uid': '123'
        },
        "isIndividual": False
    }
    assert errors == {}
