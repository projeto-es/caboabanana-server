import pytest
from caboabanana.list_items.item.models import item_schema
from caboabanana.errors.exceptions import ValidationException


def test_max_price():
    """
    GIVEN a Item with an invalid max price
    WHEN the Item is loaded
    THEN check if Exception matchs
    """
    with pytest.raises(ValidationException,
                       match="Max price may not be negative."):
        item_schema.load({
            "name": "banana",
            "quantity": 1,
            "maxPrice": -0.01,
            "isIndividual": True,
        })
