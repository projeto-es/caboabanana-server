import pytest
from caboabanana.list_items.item.models import item_schema
from caboabanana.errors.exceptions import ValidationException


def test_note():
    """
    GIVEN a Item with an invalid note
    WHEN the Item is loaded
    THEN check if Exception matchs
    """
    note = "a" * 256
    with pytest.raises(ValidationException,
                       match="Note may not have more than 255 characteres."):
        item_schema.load({
            "name": "banana",
            "quantity": 1,
            "note": note,
            "isIndividual": True,
        })
