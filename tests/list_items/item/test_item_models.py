from caboabanana.list_items.item.models import Item


def test_insert_item(database, group):
    """
    GIVEN a Group model
    WHEN a new Item is created
    THEN ensures that this item is persisted in that group listitem.
    """
    session = database.session
    assert group.list_items
    group.list_items.items.append(
        Item(False, 'Banana', 1, 'note', 0, group.author, group.list_items, 1))
    session.commit()
    assert len(group.list_items.items) == 1
    item = group.list_items.items[0]
    assert item.name == 'Banana'
    assert item.quantity == 1
    assert item.note == 'note'
    assert item.maxPrice == 0
    assert item.user == group.author
