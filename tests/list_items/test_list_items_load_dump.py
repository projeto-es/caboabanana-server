from caboabanana.list_items.item.models import Item
from caboabanana.user.models import User
from caboabanana.group.models import Group
from caboabanana.list_items.models import ListItems, list_schema


def test_load_list_items():
    """
    GIVEN a JSON of an ListItems and the ListItemsSchema
    WHEN the ListItems is loaded
    THEN check if no Exception is thrown
    """
    list_schema.load({
        "itens": [{
            "name": "banana",
            "quantity": 1,
            "isIndividual": False,
        }, {
            "name": "apple",
            "quantity": 2,
            "isIndividual": True,
        }]
    })


def test_dump_list_items():
    """
    GIVEN an ListItems
    WHEN the ListItems is dumped
    THEN check if there are no erros and data matchs
    """
    author = User("123", "foo", "foo@foo.com", "boo")
    group = Group(1234567, "banana", "#000000", author)
    group.list_items = ListItems(group)
    group.list_items.items.append(
        Item(False, 'banana', 1, 'note', 0, group.author, group.list_items, 1))
    data, errors = list_schema.dump(group.list_items)
    assert data == {
        "items": [{
            "uid": 1,
            "name": "banana",
            "quantity": 1,
            "note": "note",
            "maxPrice": 0,
            'user': {
                'access_adress': '',
                'email': 'foo@foo.com',
                'name': 'foo',
                'uid': '123'
            },
            "isIndividual": False
        }]
    }
    assert errors == {}
