from caboabanana.list_items.models import ListItems
from caboabanana.group.models import Group


def test_insert_list(database, user):
    """
    GIVEN a Group model
    WHEN a new ListItems is created
    THEN ensures that this list is persisted in database.
    """
    session = database.session
    group = Group(1, "Family", "#000000", user)
    assert group.list_items is None
    group.list_items = ListItems(group)
    session.commit()
    assert group.list_items is not None
