class FirebaseUserMock:
    def __init__(self, uid, password, display_name, email):
        self.uid = uid
        self.password = password
        self.display_name = display_name
        self.email = email


class FirebaseAuthMock:
    @staticmethod
    def create_user(**user):
        user['uid'] = '1'
        return FirebaseUserMock(**user)


class FirebaseAppMock:
    def __init__(self):
        self.default_app = FirebaseAuthMock()


class FirebaseMock:
    def __init__(self):
        self.auth = FirebaseAuthMock()

    @staticmethod
    def init_app():
        pass


firebase_mock = FirebaseMock()
