from caboabanana.group.models import Group, group_schema
from caboabanana.user.models import User
from caboabanana.group_user.models import GroupUser


def test_load_group():
    """
    GIVEN a JSON of a Group and the GroupSchema
    WHEN the Group is loaded
    THEN check if no Exception is thrown
    """
    group_schema.load({
        "uid": 1234567,
        "name": "banana",
        "color": "#000000",
        "author": {
            "uid": "123",
            "name": "banana",
            "email": "banana@banana.com"
        }
    })


def test_dump_group():
    """
    GIVEN a Group
    WHEN the Group is dumped
    THEN check if there are no erros and data matchs
    """
    author = User("123", "foo", "foo@foo.com", "boo")
    group = Group(1234567, "banana", "#000000", author)
    group.members.append(GroupUser(author, group))
    data, errors = group_schema.dump(group)
    assert data == {
        "uid": 1234567,
        "name": "banana",
        "color": "#000000",
        "author": {
            "uid": "123",
            "name": "foo",
            "email": "foo@foo.com",
            "access_adress": ""
        }
    }
    assert errors == {}
