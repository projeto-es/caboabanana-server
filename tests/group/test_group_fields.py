import pytest
from caboabanana.group.models import group_schema
from caboabanana.errors.exceptions import ValidationException


@pytest.mark.parametrize(
    "name, message",
    [(None, "Field may not be null."), ("   ", "Name may not be empty."),
     ("a" * 41, "Name may not have more than 40 characteres."),
     ("a" * 2, "Name may not have less than 3 characteres.")])
def test_name(name, message):
    """
    GIVEN a User with an invalid name
    WHEN the User is loaded
    THEN check if Exception matchs
    """
    with pytest.raises(ValidationException, match=message):
        group_schema.load({
            "uid": 1234567,
            "name": name,
            "color": "#000000",
            "author": {
                "name": "banana",
                "email": "banana@banana.com",
                "password": "banana",
            }
        })


@pytest.mark.parametrize("color, message",
                         [(None, "Field may not be null."),
                          ("   ", "Color may not be empty."),
                          ("a", "Color is not in hexadecimal format")])
def test_color(color, message):
    """
    GIVEN a User with an invalid name
    WHEN the User is loaded
    THEN check if Exception matchs
    """
    with pytest.raises(ValidationException, match=message):
        group_schema.load({
            "uid": 1234567,
            "name": "teste",
            "color": color,
            "author": {
                "name": "banana",
                "email": "banana@banana.com",
                "password": "banana",
            }
        })
