from sqlalchemy.exc import IntegrityError
from caboabanana.group.models import Group
import pytest


def test_insert_group(database, user):
    """
    GIVEN a Group model
    WHEN a new Group is created
    THEN ensures that this group is persisted in database.
    """
    assert len(Group.query.all()) == 0
    group = Group(None, "family", "blue", user)
    database.session.add(group)
    database.session.commit()
    assert len(Group.query.all()) == 1

    assert group.uid != 0


def test_insert_group_without_author(database):
    """
    GIVEN a Group model
    WHEN a new Group is created
    THEN ensures that this group is persisted in database.
    """
    group = Group(1, "family", "blue", None)
    database.session.add(group)
    with pytest.raises(IntegrityError):
        database.session.commit()


def test_not_insert_repeted_group(database, group):
    """
    GIVEN a Group model
    WHEN a new Group with repeated uid is created
    THEN ensures that this group isn't persisted in database.
    """
    database.session.add(group)
    database.session.commit()
    assert len(Group.query.all()) == 1
