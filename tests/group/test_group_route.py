from caboabanana.user.models import user_schema
from caboabanana.list_items.item.models import item_schema


def test_insert_group(client, user):
    rv = client.post(
        '/groups/',
        headers={'Authorization': user.uid},
        json={
            'name': 'family',
            'color': '#000000'
        })
    assert rv.get_json() == {
        'uid': 1,
        'name': 'family',
        'color': '#000000',
        'author': user_schema.dump(user)[0],
        'members': [user_schema.dump(user)[0]]
    }


def test_invalid_insert_group(client, user):
    rv = client.post(
        '/groups/',
        headers={'Authorization': user.uid},
        json={
            'name1': '',
            'color': '#000000'
        })
    assert rv.get_json() == {
        'message': "{'name': ['Missing data for required field.']}",
        'type': 'ValidationException'
    }


def test_get_groups(client, group_user):
    group = group_user.group
    user = group_user.user
    rv = client.get('/groups/', headers={'Authorization': user.uid})
    assert rv.get_json() == [{
        'uid': group.uid,
        'name': group.name,
        'color': group.color,
        'author': user_schema.dump(user)[0],
        'members': [user_schema.dump(user)[0]]
    }]


def test_get_group_by_id(client, group_user):
    group = group_user.group
    user = group_user.user
    rv = client.get(
        '/groups/{}'.format(group.uid), headers={'Authorization': user.uid})
    assert rv.get_json() == {
        'uid': group.uid,
        'name': group.name,
        'color': group.color,
        'author': user_schema.dump(user)[0],
        'members': [user_schema.dump(user)[0]]
    }
    rv = client.get('/groups/3', headers={'Authorization': user.uid})
    assert rv.get_json() == {}


def test_add_item(client, group_user):
    group = group_user.group
    user = group_user.user
    item = {
        'name': 'chocolate',
        'quantity': 2,
        'note': 'dark chocolate',
        'maxPrice': 55,
        'isIndividual': False
    }
    rv = client.post(
        '/groups/{}/items'.format(group.uid),
        headers={'Authorization': user.uid},
        json=item)
    item['uid'] = 1
    item['user'] = user_schema.dump(user)[0]
    assert rv.get_json() == item


def test_invalid_group(client, group_user):
    group = group_user.group
    user = group_user.user
    item = {
        'name': 'chocolate',
        'quantity': 2,
        'note': 'dark chocolate',
        'maxPrice': 55,
        'isIndividual': False
    }
    rv = client.post(
        '/groups/{}/items'.format(group.uid + 1),
        headers={'Authorization': user.uid},
        json=item)
    assert rv.get_json() == {
        'type': 'ContentNotFoundException',
        'message': 'Group not found.'
    }


def test_get_list(client, list_items):
    group = list_items.group
    user = group.author
    item = {
        'name': 'chocolate',
        'quantity': 2,
        'note': 'dark chocolate',
        'maxPrice': 55,
        'isIndividual': False
    }

    result = item_schema.dump(list_items.items, many=True)[0]

    rv = client.post(
        '/groups/{}/items'.format(group.uid),
        headers={'Authorization': user.uid},
        json=item)
    item['uid'] = 2
    item['user'] = user_schema.dump(user)[0]
    result.append(item)

    rv = client.get(
        '/groups/{}/items'.format(group.uid),
        headers={'Authorization': user.uid})

    assert rv.get_json() == result
