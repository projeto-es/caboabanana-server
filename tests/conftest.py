# pylint: disable=redefined-outer-name
import pytest
from dotenv import load_dotenv

from caboabanana import create_app
from caboabanana import db as _db
from caboabanana import extensions
from tests.mocks.firebase_mock import firebase_mock
from tests.fixtures.user_fixtures import user # NOQA
from tests.fixtures.group_fixtures import group # NOQA
from tests.fixtures.invitation_fixtures import invitation # NOQA
from tests.fixtures.group_user_fixtures import group_user # NOQA
from tests.fixtures.list_items_fixtures import list_items # NOQA


@pytest.fixture(scope="function")
def app(monkeypatch):
    load_dotenv()
    monkeypatch.setattr(extensions, 'firebase', firebase_mock)
    flask_app = create_app('caboabanana.config.Test')
    return flask_app


@pytest.fixture(scope='function')
def database(app):
    _db.app = app
    # FIXME
    from caboabanana.group.models import Group # NOQA
    from caboabanana.group_user.models import GroupUser # NOQA
    from caboabanana.invitation.models import Invitation # NOQA
    from caboabanana.list_items.models import ListItems # NOQA 
    _db.create_all()
    yield _db
    _db.session.remove()
    _db.drop_all()


@pytest.fixture(scope="function")
def client(app, database):
    flask_client = app.test_client()
    return flask_client
