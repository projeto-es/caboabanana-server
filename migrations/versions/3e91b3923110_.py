"""
Update: "Adress column added at User table"
Revision ID: 3e91b3923110
Revises: 0c43a924c0de
Create Date: 2018-06-28 13:58:29.251241

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '3e91b3923110'
down_revision = '0c43a924c0de'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('tb_users',
                  sa.Column('adress', sa.String(length=150), nullable=True))


def downgrade():
    op.drop_column('tb_users', 'adress')
