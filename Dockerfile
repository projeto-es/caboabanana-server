FROM python:3.6

COPY . /app
WORKDIR /app

ENV FLASK_APP=caboabanana
ENV FLASK_ENV=development

RUN rm .env
RUN curl -o serviceAccount.json --header "PRIVATE-TOKEN: 3MgG328iadRpAJ-3gfYu" "https://gitlab.com/projeto-es/caboabanana-server/snippets/1725828/raw"
RUN python -m pip install pipenv
RUN pipenv install --dev

EXPOSE 5000

CMD pipenv run flask db upgrade; pipenv run flask run --host='0.0.0.0'
