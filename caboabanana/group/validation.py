from caboabanana.errors.exceptions import ValidationException


def validate_color(color):
    if not color.strip():
        raise ValidationException("Color may not be empty.")
    if len(color) != 7:
        raise ValidationException("Color is not in hexadecimal format")
