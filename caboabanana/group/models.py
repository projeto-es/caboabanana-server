from marshmallow import fields, post_load
from caboabanana.errors.exceptions import ValidationException
from caboabanana.extensions import db, ma
from caboabanana.user.models import UserSchema
from caboabanana.user.validation import validate_name
from caboabanana.const_table import tb_groups, tb_users
from .validation import validate_color


class Group(db.Model):
    __tablename__ = tb_groups
    uid = db.Column('uid', db.Integer(), primary_key=True, autoincrement=True)
    uid_author = db.Column(
        db.ForeignKey('{}.uid'.format(tb_users)), nullable=False)
    name = db.Column('name', db.String(40))
    color = db.Column('color', db.String(40))
    author = db.relationship("User")
    list_items = db.relationship(
        "ListItems", uselist=False, back_populates="group")
    members = db.relationship("GroupUser", back_populates="group")
    invitations = db.relationship("Invitation", back_populates="group")

    def __init__(self, uid, name, color, author):
        self.uid = uid
        self.name = str(name)
        self.color = str(color)
        self.author = author

    def __repr__(self):
        return "<Group ID: %s>" % (self.uid)

    def __eq__(self, obj):
        return False if not isinstance(obj, Group) else self.uid == obj.uid


class GroupSchema(ma.Schema):
    uid = fields.Integer()
    name = fields.Str(required=True, validate=validate_name)
    color = fields.Str(required=True, validate=validate_color)
    author = fields.Nested(UserSchema)

    def handle_error(self, error, data):
        raise ValidationException(str(error))

    @staticmethod
    @post_load
    def make_group(data):
        return Group(**data)


group_schema = GroupSchema()
