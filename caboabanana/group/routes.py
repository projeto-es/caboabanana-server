from flask import Blueprint, request, jsonify
from caboabanana.errors.exceptions import ContentNotFoundException
from caboabanana.errors.exceptions import InvalidRequestException
from caboabanana.extensions import db
from caboabanana.guard_route import login_required
from caboabanana.user.models import user_schema
from caboabanana.list_items.models import ListItems, list_schema
from caboabanana.list_items.item.models import item_schema
from caboabanana.group_user.models import GroupUser
from .models import group_schema, Group

group = Blueprint('group', __name__, url_prefix='/groups')


@group.route('/', methods=['POST'])
@login_required
def create(user):
    if not request.is_json:
        return ('The request must be in json.', 415)

    post_object = request.get_json()
    created, error = group_schema.load(post_object)
    if error:
        pass
    created = Group(None, created['name'], created['color'], user)
    try:
        db.session.begin_nested()
        db.session.add(created)
        db.session.commit()
        created.members.append(GroupUser(user, created))
        created.list_items = ListItems(created)
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        raise e
    return jsonify(map_group_to_dic(created)), 201


@group.route('/', methods=['GET'])
@login_required
def get_all(user):
    groups = [
        map_group_to_dic(association.group) for association in user.groups
    ]
    return jsonify(groups), 200


@group.route('/<int:group_uid>', methods=['GET'])
@login_required
def get_by_id(user, group_uid):
    filtered_group = [
        association.group for association in user.groups
        if (association.uid_group == group_uid)
    ]
    return (jsonify(map_group_to_dic(filtered_group[0]))
            if len(filtered_group) == 1 else jsonify({}), 200)


@group.route('/<int:group_uid>/items', methods=['POST'])
@login_required
def add_item(user, group_uid):
    if not request.is_json:
        raise InvalidRequestException()

    post_object = request.get_json()
    item, error = item_schema.load(post_object)
    if error:
        pass
    list_items = get_group(user, group_uid).list_items
    item.list_items = list_items
    item.user = user
    list_items.items.append(item)
    db.session.add(list_items)
    db.session.commit()

    return item_schema.jsonify(item), 201


@group.route('/<int:group_uid>/items', methods=['GET'])
@login_required
def get_list(user, group_uid):
    group_aux = get_group(user, group_uid)
    list_items = list_schema.dump(group_aux.list_items)[0]
    return jsonify(list_items["items"]), 200


def get_group(user, group_uid):
    group_aux = [
        association for association in user.groups
        if association.uid_group == group_uid
    ]
    if not group_aux:
        raise ContentNotFoundException('Group not found.')
    return group_aux[0].group


def map_group_to_dic(group_to_dic):
    members = [association.user for association in group_to_dic.members]
    result, errors = group_schema.dump(group_to_dic)
    result['members'], errors = user_schema.dump(members, many=True)
    return result
