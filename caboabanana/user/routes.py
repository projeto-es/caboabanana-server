from flask import Blueprint, request
from caboabanana.extensions import firebase, db
from caboabanana.guard_route import login_required
from caboabanana.errors.exceptions import DuplicateUserException
from .models import user_schema, User

user = Blueprint('user', __name__, url_prefix='/users')


def create_user_firebase_dict(user_dict):
    new_dict = {**user_dict}
    new_dict['display_name'] = new_dict['name']
    del new_dict['name']
    return new_dict


@user.route('/', methods=['POST'])
def create():
    if not request.is_json:
        return ('The request must be in json.', 415)

    post_object = request.get_json()
    created, error = user_schema.load(post_object)
    if error:
        # TODO something here to handle errors
        pass
    firebase_user_dict = create_user_firebase_dict(created)
    check_by_email(firebase_user_dict['email'])

    user_created = firebase.auth.create_user(**firebase_user_dict)
    response = User(user_created.uid, user_created.display_name,
                    user_created.email)

    db.session.add(response)
    db.session.commit()
    return user_schema.jsonify(response), 201


def check_by_email(user_email):
    query_user = User.query.filter_by(email=user_email).first()
    if query_user is not None:
        raise DuplicateUserException()


@user.route('/', methods=['PUT'])
@login_required
def update(old_user):
    if not request.is_json:
        return ('The request must be in json.', 415)

    put_object = request.get_json()

    if validate_access_adress_json(put_object):
        user_access_adress = put_object['access_adress']
        updated_user = update_user(old_user, user_access_adress)
    else:
        # TODO something here to handle errors
        pass

    db.session.commit()
    return (user_schema.jsonify(updated_user), 201)


def update_user(old_user, user_access_adress):
    old_user.access_adress = user_access_adress
    updated_user = old_user  # User with updated attribute
    return updated_user


def validate_access_adress_json(put_object):
    ad = 'access_adress'
    return ad in put_object and put_object[ad] and put_object[ad].strip() != ""
