from caboabanana.errors.exceptions import ValidationException


def validate_name(name):
    if not name.strip():
        raise ValidationException("Name may not be empty.")
    if len(name) < 3:
        raise ValidationException("Name may not have less than 3 characteres.")
    if len(name) > 40:
        raise ValidationException(
            "Name may not have more than 40 characteres.")


def validate_email(email):
    if not email.strip():
        raise ValidationException("Email may not be empty.")
    if len(email) > 150:
        raise ValidationException(
            "Email may not have more than 150 characteres.")


def validate_password(password):
    if not password.strip():
        raise ValidationException("Password may not be empty.")
    if len(password) < 6:
        raise ValidationException(
            "Password may not have less than 6 characteres.")
    if len(password) > 40:
        raise ValidationException("Password may not have "
                                  "more than 40 characteres.")
