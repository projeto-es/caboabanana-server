import os
from caboabanana.extensions import firebase
from caboabanana.errors.exceptions import UnauthorizedRequest
from .models import User


def validate_token(token):
    if token.startswith("Bearer "):
        try:
            token = token[7:]
            decoded_token = firebase.auth.verify_id_token(token)
            uid = decoded_token['uid']
        except Exception as e:
            raise UnauthorizedRequest("Invalid token")
    elif os.getenv('FLASK_ENV') == 'development':
        uid = token
    else:
        raise UnauthorizedRequest("Invalid token")

    user = User.query.get(uid)
    if user is None:
        raise UnauthorizedRequest("Invalid token")
    return user
