from marshmallow import fields, post_load
from caboabanana.errors.exceptions import ValidationException
from caboabanana.extensions import ma, db
from caboabanana.const_table import tb_users
from .validation import validate_name, validate_email, validate_password


class User(db.Model):
    __tablename__ = tb_users
    uid = db.Column('uid', db.String(128), primary_key=True)
    name = db.Column('name', db.String(40))
    email = db.Column('email', db.String(150))
    access_adress = db.Column('adress', db.String(150))
    author_groups = db.relationship('Group')
    groups = db.relationship("GroupUser", back_populates="user")
    invitations = db.relationship(
        "Invitation",
        back_populates="remittee",
        foreign_keys="[Invitation.uid_remittee]")
    __mapper_args__ = {'exclude_properties': ['password']}

    def __init__(self, uid, name, email, password="", access_adress=""):
        self.uid = str(uid)
        self.name = str(name)
        self.email = str(email)
        self.password = str(password)
        self.access_adress = str(access_adress)

    def __repr__(self):
        return "<User ID: %s>" % (self.uid)

    def __eq__(self, obj):
        return False if not isinstance(obj, User) else self.uid == obj.uid


class UserSchema(ma.Schema):

    uid = fields.Str()
    name = fields.Str(required=True, validate=validate_name)
    email = fields.Email(required=True, validate=validate_email)
    password = fields.Str(
        load_only=True, validate=validate_password)
    access_adress = fields.Str()

    def handle_error(self, error, data):
        raise ValidationException(str(error))

    @staticmethod
    @post_load
    def make_user(data):
        return User(**data)


user_schema = UserSchema()
