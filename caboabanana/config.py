import os
import logging


class BaseConfig:
    """Base Flask config variables."""
    DEBUG = False

    FIREBASE_CONFIG = os.getenv('FIREBASE_AUTH_PATH')

    LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    LOGGING_LOCATION = 'caboabanana.log'
    LOGGING_LEVEL = logging.DEBUG

    MYSQL_HOST = os.getenv('CABOABANANA_MYSQL_HOST')
    MYSQL_USER = os.getenv('CABOABANANA_MYSQL_USER')
    MYSQL_PASSWORD = os.getenv('CABOABANANA_MYSQL_PASSWORD')
    MYSQL_DATABASE = os.getenv('CABOABANANA_MYSQL_DATABASE')

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{}:{}@{}/{}'.format(
        MYSQL_USER, MYSQL_PASSWORD, MYSQL_HOST, MYSQL_DATABASE)


class Test(BaseConfig):
    """Test Flask config variables."""
    DEBUG = True
    TESTING = True

    MYSQL_HOST = os.getenv('CABOABANANA_MYSQL_TEST_HOST')
    MYSQL_USER = os.getenv('CABOABANANA_MYSQL_TEST_USER')
    MYSQL_PASSWORD = os.getenv('CABOABANANA_MYSQL_TEST_PASSWORD')
    MYSQL_DATABASE = os.getenv('CABOABANANA_MYSQL_TEST_DATABASE')

    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{}:{}@{}/{}'.format(
        MYSQL_USER, MYSQL_PASSWORD, MYSQL_HOST, MYSQL_DATABASE)


class Development(BaseConfig):
    """Development Flask config variables."""
    DEBUG = True
