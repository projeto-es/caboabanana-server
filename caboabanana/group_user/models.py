from caboabanana.extensions import db
from caboabanana.const_table import tb_groups_users, tb_users, tb_groups


class GroupUser(db.Model):
    __tablename__ = tb_groups_users
    uid_user = db.Column(
        db.String(128),
        db.ForeignKey('{}.uid'.format(tb_users)),
        primary_key=True)
    uid_group = db.Column(
        db.Integer(),
        db.ForeignKey('{}.uid'.format(tb_groups)),
        primary_key=True)
    user = db.relationship("User", back_populates="groups")
    group = db.relationship("Group", back_populates="members")

    def __init__(self, user, group):
        self.user = user
        self.group = group

    def __repr__(self):
        return "<Group ID: %s User ID: %s>" % (self.uid_group, self.uid_user)

    def __eq__(self, ob):
        return False if not isinstance(
            ob, GroupUser
        ) else self.uid_user == ob.uid_user and self.uid_group == ob.uid_group
