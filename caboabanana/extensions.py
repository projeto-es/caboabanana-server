from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_firebase import FlaskFirebase

ma = Marshmallow()
db = SQLAlchemy()
firebase = FlaskFirebase()
migrate = Migrate()
