from flask import Flask, jsonify
from caboabanana.extensions import ma, db, firebase, migrate
from caboabanana.guard_route import login_required


def create_app(config='caboabanana.config.Development'):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(config)

    with app.app_context():
        ma.init_app(app)
        db.init_app(app)
        _register_blueprints(app)
        _register_extensions(app)
        # FIXME
        from caboabanana.group.models import Group # NOQA
        from caboabanana.group_user.models import GroupUser # NOQA
        from caboabanana.invitation.models import Invitation # NOQA
        db.create_all()

    @app.route('/')
    def hello():
        return 'Cabo a Banana is up and running!'

    @app.route('/secure', methods=['POST'])
    @login_required
    def secure(user):
        return jsonify({'username': user.name})

    return app


def _register_extensions(app):
    ma.init_app(app)
    db.init_app(app)
    firebase.init_app(app)
    migrate.init_app(app, db)


def _register_blueprints(app):
    from caboabanana.user.routes import user
    from caboabanana.group.routes import group
    from caboabanana.errors.handlers import errors

    app.register_blueprint(errors)
    app.register_blueprint(group)
    app.register_blueprint(user)
