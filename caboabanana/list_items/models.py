from marshmallow import fields, post_load
from caboabanana.extensions import db, ma
from caboabanana.const_table import tb_list_items, tb_groups
from caboabanana.errors.exceptions import ValidationException
from caboabanana.list_items.item.models import ItemSchema


class ListItems(db.Model):
    __tablename__ = tb_list_items
    uid = db.Column(db.Integer(), autoincrement=True, primary_key=True)
    uid_group = db.Column(
        db.Integer(),
        db.ForeignKey('{}.uid'.format(tb_groups)),
        primary_key=True)
    group = db.relationship("Group", back_populates="list_items")
    items = db.relationship("Item", back_populates="list_items")

    def __init__(self, group):
        self.group = group

    def __repr__(self):
        return "<ListItems ID: %s Group ID: %s>" % (self.uid, self.uid_group)

    def __eq__(self, ob):
        return False if not isinstance(
            ob, ListItems
        ) else self.uid == ob.uid and self.uid_group == ob.uid_group


class ListItemSchema(ma.Schema):
    items = fields.Nested(ItemSchema, dump_only=True, many=True)

    def handle_error(self, error, data):
        raise ValidationException(str(error))

    @staticmethod
    @post_load
    def make_list(data):
        return ListItems(**data)


list_schema = ListItemSchema()
