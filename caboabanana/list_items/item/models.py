from marshmallow import fields, post_load
from caboabanana.extensions import db, ma
from caboabanana.const_table import tb_item, tb_users, tb_list_items
from caboabanana.errors.exceptions import ValidationException
from caboabanana.user.validation import validate_name
from caboabanana.user.models import UserSchema
from caboabanana.list_items.item.validation import (
    validate_note, validate_max_price, validate_quantity)


class Item(db.Model):
    __tablename__ = tb_item
    uid = db.Column(db.Integer(), autoincrement=True, primary_key=True)
    uid_list = db.Column(
        db.ForeignKey('{}.uid'.format(tb_list_items)), primary_key=True)
    uid_user = db.Column(
        db.ForeignKey('{}.uid'.format(tb_users)), nullable=False)
    list_items = db.relationship("ListItems")
    user = db.relationship("User")
    name = db.Column(db.String(40))
    quantity = db.Column(db.Integer())
    note = db.Column(db.String(255))
    maxPrice = db.Column(db.Float())
    isIndividual = db.Column(db.Boolean())

    def __init__(self,
                 isIndividual,
                 name,
                 quantity,
                 note=None,
                 maxPrice=None,
                 user=None,
                 list_items=None,
                 uid=None):
        self.uid = uid
        self.list_items = list_items
        self.name = name
        self.quantity = quantity
        self.note = note
        self.maxPrice = maxPrice
        self.user = user
        self.isIndividual = isIndividual

    def __repr__(self):
        return "<Item ID: %s List ID: %s >" % (self.uid, self.uid_list)

    def __eq__(self, ob):
        return False if not isinstance(
            ob, Item) else self.uid == ob.uid and self.uid_list == ob.uid_list


class ItemSchema(ma.Schema):
    _model__ = Item
    uid = fields.Integer(dump_only=True)
    name = fields.Str(required=True, validate=validate_name)
    quantity = fields.Integer(required=True, validate=validate_quantity)
    note = fields.Str(validate=validate_note)
    maxPrice = fields.Float(validate=validate_max_price)
    user = fields.Nested(UserSchema, dump_only=True)
    isIndividual = fields.Boolean(required=True)

    def handle_error(self, error, data):
        raise ValidationException(str(error))

    @post_load
    def make_item(self, data):
        return self._model__(**data)


item_schema = ItemSchema()
