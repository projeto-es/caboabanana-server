from caboabanana.errors.exceptions import ValidationException


def validate_quantity(quantity):
    if quantity == 0:
        raise ValidationException("Quantity may not be zero.")
    if quantity < 0:
        raise ValidationException("Quantity may not be negative.")


def validate_note(note):
    if note and len(note) > 255:
        raise ValidationException("Note may not have more than"
                                  " 255 characteres.")


def validate_max_price(max_price):
    if max_price < 0:
        raise ValidationException("Max price may not be negative.")
