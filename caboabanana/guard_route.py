from functools import wraps
from flask import request
from caboabanana.user.validation_token import validate_token


def login_required(f):
    @wraps(f)
    def decorated_function(**args):
        try:
            token = request.headers['Authorization']
            user = validate_token(token)
            return f(user, **args)
        except Exception as error:
            raise error
    return decorated_function
