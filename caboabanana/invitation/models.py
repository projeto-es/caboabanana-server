from marshmallow import fields, post_load
from caboabanana.errors.exceptions import ValidationException
from caboabanana.extensions import db, ma
from caboabanana.user.models import User, UserSchema
from caboabanana.group.models import Group, GroupSchema


class Invitation(db.Model):
    __tablename__ = 'tb_invitations'
    uid_remittee = db.Column(
        db.String(128),
        db.ForeignKey('{}.uid'.format(User.__tablename__)),
        primary_key=True)
    uid_sender = db.Column(
        db.String(128), db.ForeignKey('{}.uid'.format(User.__tablename__)))
    uid_group = db.Column(
        db.ForeignKey('{}.uid'.format(Group.__tablename__)), primary_key=True)
    accepted = db.Column('accepted', db.Boolean())
    group = db.relationship("Group", back_populates="invitations")
    remittee = db.relationship(
        "User", back_populates="invitations", foreign_keys=uid_remittee)
    sender = db.relationship("User", foreign_keys=uid_sender)

    def __init__(self, sender, remittee, group):
        self.sender = sender
        self.remittee = remittee
        self.group = group

    def __repr__(self):
        return "<Group ID: %s Remette ID: %s>" % (self.uid_group,
                                                  self.uid_remittee)

    def __eq__(self, ob):
        return False if not isinstance(
            ob, Invitation
        ) else self.uid_user == ob.uid_user and self.uid_group == ob.uid_group


class InvitationSchema(ma.Schema):

    uid_remittee = fields.Str(load_only=True)
    uid_sender = fields.Str(required=True, load_only=True)
    uid_group = fields.Str(required=True, load_only=True)
    remittee = fields.Nested(UserSchema, dump_only=True)
    sender = fields.Nested(UserSchema, dump_only=True)
    group = fields.Nested(GroupSchema, dump_only=True)

    def handle_error(self, error, data):
        raise ValidationException(str(error))

    @staticmethod
    @post_load
    def make_invitation(data):
        return Invitation(**data)


invitation_schema = InvitationSchema()
