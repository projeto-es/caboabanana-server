import traceback

from flask import Blueprint, jsonify
from flask import current_app as app

from . import exceptions

errors = Blueprint('errors', __name__)


@errors.app_errorhandler(exceptions.CaboaBanana)
def handle_error(error):
    app.logger.error(traceback.format_exc())
    response = {'type': error.__class__.__name__, 'message': error.message}
    return jsonify(response), error.status


@errors.app_errorhandler(Exception)
def handle_unexpected_error(error):
    app.logger.error(traceback.format_exc())

    status_code = 500
    response = {
        'error': {
            'type':
            'UnexpectedException',
            'message':
            error.message
            if error['message'] is not None else 'Unexpected error'
        }
    }

    return jsonify(response), status_code
