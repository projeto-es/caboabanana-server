class CaboaBanana(Exception):
    def __init__(self, message="Internal Server Error.", status=500):
        Exception.__init__(self)
        self.status = status
        self.message = message

    def to_dict(self):
        return {"status": self.status, "message": self.message}

    def __str__(self):
        return self.message


class ValidationException(CaboaBanana):
    def __init__(self, message, status=400):
        CaboaBanana.__init__(self, message, status)


class DuplicateUserException(CaboaBanana):
    def __init__(self,
                 message="An existing account with this email already exists.",
                 status=409):
        CaboaBanana.__init__(self, message, status)


class ContentNotFoundException(CaboaBanana):
    def __init__(self, message="Content Not Found.", status=404):
        CaboaBanana.__init__(self, message, status)


class InvalidRequestException(CaboaBanana):
    def __init__(self, message="The request must be in json.", status=415):
        CaboaBanana.__init__(self, message, status)


class UnauthorizedRequest(CaboaBanana):
    def __init__(self, message="This is an unauthorized request", status=401):
        CaboaBanana.__init__(self, message, status)
