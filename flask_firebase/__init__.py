import firebase_admin
from firebase_admin import credentials, auth
from flask import current_app, _app_ctx_stack


class FlaskFirebase(object):
    def __init__(self, app=None):
        self.app = app
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        app.teardown_appcontext(self.teardown)

    def connect(self):
        try:
            app = firebase_admin.get_app()
        except ValueError:
            cred = credentials.Certificate(
                current_app.config['FIREBASE_CONFIG'])
            app = firebase_admin.initialize_app(cred)
        return app

    def teardown(self, exception):
        ctx = _app_ctx_stack.top
        if hasattr(ctx, 'firebase_app'):
            ctx.firebase_app.delete_app()

    @property
    def auth(self):
        self.connect()
        return auth
